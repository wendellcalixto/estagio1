import requests 
import json

cnpj = input("Insira o cnpj: ")

response = requests.get("https://www.receitaws.com.br/v1/cnpj/"+cnpj)

objeto = json.loads(response.text)

if 'message' in objeto:
    print (objeto['message'])
else:
    print("")
    print("Atividade Principal")
    for item in objeto['atividade_principal']:
        print("\t",item['code'], " ", item['text'])

    print("")
    print("Atividades Secundárias")
    for item in objeto['atividades_secundarias']:
        print("\t",item['code'], "", item['text'])

    print("")

    print("Nome:", objeto['nome'])
    print("UF:", objeto['uf'])
    print("Telefone:", objeto['telefone'])
    print("Email:", objeto['email'])
    print("Data de Abertura:", objeto['abertura'])

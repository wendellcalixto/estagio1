import string
import sqlite3
import os
from ProvaEBI_DAO import SQLiteDAO
from ProvaEBI_Requests import RequestCNPJ

clear = lambda: os.system('cls')

class Del:
  def __init__(self, keep=string.digits):
    self.comp = dict((ord(c),c) for c in keep)
  def __getitem__(self, k):
    return self.comp.get(k)

DD = Del()

connection = SQLiteDAO()
connection.Connect()

escolha_principal = 999
while(escolha_principal != 0):
    clear()
    print("")
    print("MENU PRINCIPAL")
    print("")
    print("1 - Empresas")
    print("2 - Serviços")
    print("3 - Clientes")
    print("")
    print("0 - SAIR")
    print("")
    escolha_principal = int(input("Escolha a opção desejada: "))
    print("")

    if (escolha_principal == 1):
        escolha_empresa = 999
        while(escolha_empresa != 0):
            clear()
            print("")
            print("MENU EMPRESA ")
            print("")
            print("1 - Cadastrar")
            print("2 - Alterar")
            print("3 - Remover")
            print("4 - Listar")
            print("5 - Cadastro Automático (Request)")
            print("0 - VOLTAR PARA O MENU PRINCIPAL")
            print("")
            escolha_empresa = int(input("Escolha a opção desejada: "))
            print("")

            if (escolha_empresa == 1):
                clear()
                print("")
                print("CADASTRAR EMPRESA ")
                print("")
                cnpj_empresa = input("Informe o CNPJ: ")
                nome_empresa = input("Informe o nome: ")
                endereco_empresa = input("Informe o endereço: ")
                email_empresa = input("Informe o email: ")
                print("")               
                
                try:
                    connection.Execute("INSERT INTO Empresa (CNPJ, Nome, Endereco, Email) VALUES (?,?,?,?)",[cnpj_empresa, nome_empresa, endereco_empresa, email_empresa])
                    print("Empresa do CNPJ "+ email_empresa+ " inserida com sucesso." )
                    
                except:
                    print("Não foi possível inserir a empresa." )   
                    
                print("")
                input("Pressione enter para voltar ao menu EMPRESA...")

            elif (escolha_empresa == 2):
                clear()
                print("")
                print("ALTERAR EMPRESA ")
                print("") 

                editar_empresa = input("Informe o cnpj da empresa a ser alterada: ")
                
                connection.Execute("SELECT CNPJ, Nome, Endereco, Email FROM Empresa WHERE CNPJ = ?", [editar_empresa])
                rows = connection.FechAll()
                print("")
                if(len(rows) == 0):
                    print("Nenhuma empresa encontrada para o CNPJ " + editar_empresa)
                else:
                    for row in rows:
                        print("DADOS ATUAIS DA EMPRESA")
                        print("CNPJ:", row[0])
                        print("Nome:", row[1])
                        print("Endereço:", row[2])
                        print("Email:", row[3])
                        print("")
                        break

                    print("NOVOS DADOS PARA A EMPRESA")
                    nome_empresa = input("Informe o nome: ")
                    endereco_empresa = input("Informe o endereço: ")
                    email_empresa = input("Informe o email: ")

                    connection.Execute("UPDATE Empresa SET Nome = ?, Endereco = ?, Email = ? WHERE CNPJ = ?",[nome_empresa, endereco_empresa, email_empresa, editar_empresa])

                print("")
                print("Empresa do CNPJ "+ editar_empresa+ " alterada com sucesso." )
                print("")
                input("Pressione enter para voltar ao menu EMPRESA...")
                
            elif (escolha_empresa == 3):
                clear()
                print("")
                print("REMOVER EMPRESA ")
                print("")     
                delete_empresa = input("Informe o cnpj da empresa a ser removida: ")
                
                connection.Execute("DELETE FROM Empresa WHERE CNPJ = ?", [delete_empresa])

                print("")
                if(connection.cursor.rowcount == 0):
                    print("Nenhuma empresa encontrada para o CNPJ "+ delete_empresa+ "." )
                else:               
                    print("Empresa do CNPJ "+ delete_empresa+ " removida com sucesso." )
                print("")
                input("Pressione enter para voltar ao menu EMPRESA...")

            elif (escolha_empresa == 4):
                clear()
                print("")
                print("LISTAR EMPRESAS")
                print("")

                connection.Execute("SELECT CNPJ, Nome, Endereco, Email FROM Empresa", [])
                rows = connection.FechAll()

                for row in rows:
                    print("CNPJ:", row[0])
                    print("Nome:", row[1])
                    print("Endereço:", row[2])
                    print("Email:", row[3])
                    print("")

                input("Pressione enter para voltar ao menu EMPRESA...")

                #Serviços aqui:

            elif (escolha_empresa == 5):
                clear()
                print("")
                print("CADASTRAR EMPRESA AUTOMÁTICAMENTE ")
                print("")
                cnpj_empresa = input("Informe o CNPJ: ")
                print("")  
                
                try:
                    request = RequestCNPJ()
                    retorno = request.Consultar(cnpj_empresa)  
                    if 'message' in retorno:
                        print (retorno['message'])
                    else:
                        connection.Execute("INSERT INTO Empresa (CNPJ, Nome, Endereco, Email) VALUES (?,?,?,?)",[cnpj_empresa, retorno['nome'] , retorno['logradouro'] , retorno['email']])

                        for item in retorno['atividade_principal']:
                            code= item['code']                            
                            connection.Execute("INSERT INTO Servico (Nome, Codigo, Valor, CNPJ) VALUES (?,?,?,?)", [item['text'], code.translate(DD), 0 , cnpj_empresa])

                        for item in retorno['atividades_secundarias']:
                            code= item['code']                            
                            connection.Execute("INSERT INTO Servico (Nome, Codigo, Valor, CNPJ) VALUES (?,?,?,?)", [item['text'], code.translate(DD), 0 , cnpj_empresa])

                        print("Empresa do CNPJ "+ cnpj_empresa+ " inserida com sucesso." )
                except:
                    print("Não foi possível inserir a empresa." )   
                   
                print("")
                input("Pressione enter para voltar ao menu EMPRESA...")
    if (escolha_principal == 2):
        escolha_empresa = 999
        while(escolha_empresa != 0):
            clear()
            print("")
            print("MENU SERVIÇOS")
            print("")
            print("1 - Cadastrar")
            print("2 - Alterar")
            print("3 - Remover")
            print("4 - Listar")
            print("0 - VOLTAR PARA O MENU PRINCIPAL")
            print("")
            escolha_empresa = int(input("Escolha a opção desejada: "))
            print("")

            if (escolha_empresa == 1):
                clear()
                print("")
                print("CADASTRAR SERVIÇOS")
                print("")
                nome_servico = input("Nome: ")
                codigo_servico = input("Código: ")
                valor_servico = input("Valor: ")
                cnpj_servico = input("CNPJ da empresa que presta o serviço: ")
                print("")
                
                try:
                    connection.Execute("INSERT INTO Servico (Nome,Codigo,Valor,CNPJ) VALUES (?,?,?,?)",[nome_servico,codigo_servico,valor_servico,cnpj_servico])

                    connection.Execute("SELECT Nome FROM Empresa WHERE CNPJ = ?", [cnpj_servico])
                    nome_da_empresa = ""
                    rows = connection.FechAll()    
                    for row in rows:
                        nome_da_empresa = row[0]
                        break
                    print("O serviço '"+ nome_servico+ "' cadastrado com sucesso e vinculado a empresa '" +nome_da_empresa+"'.")
                except:
                    print("Não foi possível inserir o serviço." )   

                print("")
                input("Pressione enter para voltar ao menu SERVIÇOS...")

            elif (escolha_empresa == 2):
                clear()
                print("")
                print("ALTERAR SERVIÇO")
                print("")   

                editar_serviço = input("Informe a ID a ser alterada: ")                
                
                connection.Execute("SELECT ID, Nome, Codigo, Valor, CNPJ FROM Servico WHERE ID = ?", [editar_serviço])
                
                rows = connection.FechAll()               
                print("")
                if(len(rows) == 0):
                    print("Nenhum serviço encontrado com ID:" + editar_serviço)
                else:
                    for row in rows:
                        print("DADOS ATUAIS DO SERVIÇO")
                        print("ID:", row[0])
                        print("Nome:", row[1])
                        print("Codigo:", row[2])
                        print("Valor:", row[3])
                        print("CNPJ:", row[4])
                        print("")
                        break

                    print("NOVOS DADOS PARA O SERVIÇO")
                    nome_servico = input("Informe o novo nome: ")
                    codigo_servico = input("Informe o novo codigo: ")
                    valor_servico = input("Informe o novo valor: ")
                    cnpj_servico = input("Infome o novo CNPJ: ")

                    connection.Execute("UPDATE Servico SET Nome = ?, Codigo = ?, Valor = ?, CNPJ = ? WHERE ID = ?",[nome_servico, codigo_servico, valor_servico, cnpj_servico, editar_serviço])
                                    
                print("")
                print("Serviço ID "+ editar_serviço+ " alterado com sucesso." )
                print("")
                input("Pressione enter para voltar ao menu SERVIÇOS...")

            elif (escolha_empresa == 3):
                clear()
                print("")
                print("REMOVER SERVIÇO")
                print("")     
                delete_servico = input("Informe a ID do serviço a ser removido: ")
                                              
                connection.Execute("DELETE FROM Servico WHERE ID= ?", [delete_servico])                

                print("")
                if(connection.cursor.rowcount == 0):
                    print("Nenhum serviço encontrado com a ID "+ delete_servico+ "." )
                else:               
                    print("Serviço com a ID "+ delete_servico+ " removido com sucesso." )
                print("")
                input("Pressione enter para voltar ao menu SERVIÇO...")

            elif (escolha_empresa == 4):
                clear()
                print("")
                print("LISTAR SERVIÇOS")
                print("")

                connection.Execute("SELECT ID, Nome, Codigo, Valor, CNPJ FROM Servico", [])
                rows = connection.FechAll()

                for row in rows:
                    print("ID:", row[0])
                    print("Nome:", row[1])
                    print("Codigo:", row[2])
                    print("Valor:", row[3])
                    print("CNPJ:", row[4])
                    print("")

                input("Pressione enter para voltar ao menu SERVIÇO...")

                #CLiENTES

    if (escolha_principal == 3):
            escolha_empresa = 999
            while(escolha_empresa != 0):
                clear()
                print("")
                print("MENU CLIENTES ")
                print("")
                print("1 - Cadastrar")
                print("2 - Alterar")
                print("3 - Remover")
                print("4 - Listar")
                print("0 - VOLTAR PARA O MENU PRINCIPAL")
                print("")
                escolha_empresa = int(input("Escolha a opção desejada: "))
                print("")

                if (escolha_empresa == 1):
                    clear()
                    print("")
                    print("CADASTRAR CLIENTES ")
                    print("")
                    id_cliente = input ("Informe a ID: ")
                    cnpj_cliente = input("Informe o CNPJ: ")
                    nome_cliente = input("Informe o nome: ")
                    telefone_cliente = input("Informe o telefone: ")
                    print("")               
                    
                    try:   
                        connection.Execute("INSERT INTO Cliente ( ID, CNPJ, Nome, Telefone) VALUES (?,?,?,?)",[ id_cliente, cnpj_cliente, nome_cliente, telefone_cliente])
                                                                    
                        print("Cliente " + nome_cliente + " inserido com sucesso." )
                        
                    except:
                        print("Não foi possível inserir o cliente." )   
                        
                    print("")
                    input("Pressione enter para voltar ao menu CLIENTE...")

                elif (escolha_empresa == 2):
                    clear()
                    print("")
                    print("ALTERAR CLIENTES ")
                    print("") 

                    editar_cliente = input("Informe o ID do cliente a ser alterado: ")                    
                    
                    connection.Execute("SELECT ID, CNPJ, Nome, Telefone FROM Cliente WHERE ID = ?", [editar_cliente])
                    
                    rows = connection.FechAll()               
                    print("")
                    if(len(rows) == 0):
                        print("Nenhum cliente encontrado para a ID " + editar_cliente)
                    else:
                        for row in rows:
                            print("DADOS ATUAIS DA EMPRESA")
                            print("ID:", row[0])
                            print("CNPJ:", row[1])
                            print("Nome:", row[2])
                            print("Telefone:", row[3])
                            print("")
                            break

                        print("NOVOS DADOS PARA O CLIENTE")
                        cnpj_cliente = input("Informe o CNPJ: ")
                        nome_cliente = input("Informe o nome: ")
                        telefone_cliente = input("Informe o telefone: ")

                        connection.Execute("UPDATE Cliente SET CNPJ = ?, Nome = ?, Telefone = ? WHERE ID = ?",[cnpj_cliente, nome_cliente, telefone_cliente, editar_cliente])
                                            
                    print("")
                    print("Clinte com ID "+ editar_cliente+ " alterado com sucesso." )
                    print("")
                    input("Pressione enter para voltar ao menu CLIENTE...")
                    
                elif (escolha_empresa == 3):
                    clear()
                    print("")
                    print("REMOVER CLIENTE ")
                    print("")     
                    delete_cliente = input("Informe a ID do cliente a ser removido: ")
                    
                    connection.Execute("DELETE FROM Cliente WHERE ID = ?", [delete_cliente])
                                      
                    if(connection.cursor.rowcount == 0):
                        print("Nenhum cliente encontrado para a ID "+ delete_cliente + "." )
                    else:               
                        print("Cliente da ID "+ delete_cliente+ " removido com sucesso." )
                    print("")
                    input("Pressione enter para voltar ao menu CLIENTES...")

                elif (escolha_empresa == 4):
                    clear()
                    print("")
                    print("LISTAR CLIENTES")
                    print("")

                    connection.Execute("SELECT ID, CNPJ, Nome, Telefone FROM Cliente", [])
                    rows = connection.FechAll()

                    for row in rows:
                        print("ID:", row[0])
                        print("CNPJ:", row[1])
                        print("Nome:", row[2])
                        print("Telefone:", row[3])
                        print("")

                    input("Pressione enter para voltar ao menu CLIENTES...")

                    

import sqlite3

class SQLiteDAO:
    def __init__(self):
        self.database = "ProvaEBI.db"
        self.connection = None
        self.cursor = None

    def Connect(self):
        self.connection = sqlite3.connect(self.database)
        self.cursor = self.connection.cursor()  
        self.cursor.execute("PRAGMA foreign_keys = ON")
        self.connection.commit()        

    def Execute(self, sql, parameters):
        self.cursor = self.connection.cursor()  
        self.cursor.execute(sql, parameters)
        self.connection.commit()

    def Disconnect(self):
        self.connection.close()

    def FechAll(self):
        return self.cursor.fetchall()
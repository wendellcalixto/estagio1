import requests 
import json

class RequestCNPJ:
    def __init__(self):
        self.uri = "https://www.receitaws.com.br/v1/cnpj/"

    def Consultar(self, cnpj):
        response = requests.get(self.uri + cnpj)
        return json.loads(response.text)